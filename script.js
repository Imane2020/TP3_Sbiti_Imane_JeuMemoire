'use strict'
//ctrl alt d documentation JSDoc installer le module Document this
monEvenement()
/**
 *c'est la fontion principale qui execute le script avec le clique du bouton
 *
 */
function monEvenement(){
  const lancerjeu = document.getElementById('jouer')
  lancerjeu.addEventListener('click', afficherLeJeu)
}
/**
 * c'est la fontion qui appel les valeurs des champs remplis par le joueur (nombre de cartes et nom)
 */
function parametresrentres(){
    const champNombreBoutons = document.getElementById('nombre')
    let nombreBoutons = champNombreBoutons.value
    nombreBoutons = parseInt(nombreBoutons)
    const nomjoueur = document.getElementById('nom')
    let nom = nomjoueur.value
    return {nombre : nombreBoutons, nom : nom}
}

let carteTournees = []
let pairesTrouvees = 0
/**
 *c'est la fonction qui permet de remplir un tableau avec le nombre rentré par le joueur,
 *ça permet aussi de mélanger les cartes
 * @return {*} retourne le tableau des cartes mélangées
 */
function tableauAleatoire(){
  const parametres = parametresrentres()
  const nombrePaires = parametres.nombre
  const nbTours = nombrePaires // J'avais fait une division par deux ici
  const tableauCartes = []
  for (let i = 0; i < nbTours; i++) {
    tableauCartes.push(i)
    tableauCartes.push(i)
  }
  const tableauCartesMelangees = []
  while(tableauCartes.length > 0) {
    const index = Math.floor(Math.random() * tableauCartes.length)
    tableauCartesMelangees.push(tableauCartes[index])
    tableauCartes.splice(index, 1)
  }
  return tableauCartesMelangees
}
/**
 * Fonction qui sert à masquer le formulaire et afficher le bloc du jeu, la fontion permet aussi de faire certaines opérations sur les boutons du jeu
 * @param {*} e c'est un prevent defeult pour éviter les erreurs 
 */
function afficherLeJeu(e){
  const parametres = parametresrentres()
  const monNombre = parametres.nombre
  const monNom = parametres.nom
  let ul = document.getElementById("message")
  ul.innerHTML = ""
  let message = []
      if(isNaN(monNombre)){
        message.push("Nombre obligatoire")
      }else if(monNombre < 2 || monNombre > 10){
        message.push("Vous devez saisir un nombre entre 2 et 10")
      }else if( monNom === ''){
        message.push("Nom obligatoire")
      }
      if (message.length > 0){
          e.preventDefault()
          for (let i = 0 ; i < message.length ; i++){
            const li = document.createElement("li")
            message = message[i]
            const texte = document.createTextNode(message)
            li.appendChild(texte)
            ul.appendChild(li)
          }
        }else{
          document.getElementById('monFormulaire').style.display = 'none'
          setInterval(compteurtemps, 1000)
          const newtext = document.createTextNode('Bonne chance ' + monNom + '  !')
          const p1 = document.getElementById("bonnechance")
          p1.appendChild(newtext);
          setTimeout(finDuJeu, 50000) /* j'ai mis 50 secondes au lieu de 5 mins*/
          const cartesMelangees = tableauAleatoire()
          console.log(cartesMelangees)
          const jeux = document.getElementById('jeu')
          jeux.innerHTML = ""
          for (let i = 0 ; i < cartesMelangees.length ; i++){
              var t = document.createTextNode('X')
              const bouton = document.createElement('button')
              bouton.classList.add("btn");
              bouton.style.color = 'white'
              bouton.style.backgroundColor = '#FF005C'
              bouton.style.margin = '20px'
              bouton.style.fontFamily = 'Quando'
              bouton.style.fontWeight = '500'
              bouton.style.fontSize = '30px'
              bouton.style.border = "thick solid #00C2FF"
              bouton.style.borderRadius = '10px'
              bouton.style.height = '100px'
              bouton.style.width = '100px'
              bouton.setAttribute('chiffre', cartesMelangees[i])
              bouton.addEventListener('click', operationbouton)
              bouton.appendChild(t)
              jeux.appendChild(bouton) 
          }
      }
  }
/**
 *c'est ici que le jeu s'anime, cette fonction permet de vérifier si les cartes sont pareil 
 *si c'est le cas un tableau se rempli et un message de vistoire s'affiche sinon les cartes s'inversent
 * @param {*} e c'est un paramètre qui appel le contenu d'un bouton
 */
function operationbouton(e){
    const monBouton = e.target
    const nombreDeLaCarte = monBouton.getAttribute('chiffre')
    console.log (nombreDeLaCarte)
    monBouton.textContent = nombreDeLaCarte
    carteTournees.push(e.target)
    if (carteTournees.length === 2){
        let carte1 = carteTournees[0].getAttribute('chiffre')
        let carte2 = carteTournees[1].getAttribute('chiffre')
        if (carte1 === carte2){
            carteTournees[0].disabled = true
            carteTournees[1].disabled = true
            pairesTrouvees += 1
            carteTournees = []
            const parametres = parametresrentres()
            const nombrePaires = parametres.nombre
            if (pairesTrouvees === nombrePaires){
              setTimeout(victoire,1000)
            }
        }else {
            setTimeout(inverser,1000)
        }
    }
}
/**
 *c'est le message victoire
 *
 */
function victoire(){
    alert('Bingooooo !! Victoire')
}
/**
 *fonction qui permet d'inverser les cartes s'ils ne sont pas pareil aprées une seconde
 *
 */
function inverser(){
    carteTournees[0].textContent = 'X'
    carteTournees[1].textContent = 'X'
    carteTournees = []
}
/**
 *Fonction qui retourne le compteur du jeu, maximum 5 mins
 */
function finDuJeu(){
    const partiedujeu = document.getElementById('jeu')
    partiedujeu.style.display = "none"
    alert('Oups ! Fin de la partie, vous avez dépasser 5 minutes')
}
/**
 *c'est le compteur du jeu
 */
function compteurtemps(){
    const compteur = document.getElementById('compteur')
    let contenu = parseInt(compteur.textContent)
    if(isNaN(contenu)){
      contenu = 0
    }else {
      contenu = contenu + 1 
    }
    compteur.textContent = contenu
    if(contenu === 5){
      clearInterval(compteurtemps)
    }
}